#!/usr/bin/env python3
from flask import Flask

app = Flask(__name__)

@app.route('/')
def landing():
    return '<h1>Hello beautiful world!</h1>'

@app.route('/info')
def info():
    return 'Are you lost?'

if __name__ == '__main__':
    app.run(debug=True, port=5000, host='0.0.0.0')
